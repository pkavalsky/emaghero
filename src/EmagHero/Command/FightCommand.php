<?php

namespace EmagHero\Command;

use EmagHero\Console\Output;
use EmagHero\Console\Input;
use EmagHero\Core\AbstractCommand;
use EmagHero\Mapper\WildBeastMapper;
use EmagHero\Mapper\HeroMapper;
use EmagHero\Event\AttackEvent;
use EmagHero\Event\BattleStartEvent;
use EmagHero\Event\EndOfTurnEvent;
use EmagHero\Event\BeginningOfTurnEvent;
use EmagHero\Event\GameOverEvent;

class FightCommand extends AbstractCommand
{
    public function execute ( Input $input, Output $output ) : void
    {
        $eventManager = $this->getDI()->get('eventmanager');
        $resolver = $this->getDI()->get('resolver');
        $hero = (new HeroMapper)->create();
        $wildBeast = (new WildBeastMapper)->create();
        $eventManager->dispatch( new BattleStartEvent( $hero, $wildBeast ) );
        $i = 1;
        while( false === $resolver->isGameOver ( $hero, $wildBeast, $i ) )
        {
            if( $hero->getIsAttacker() )
            {
                $attacker = $hero;
                $defender = $wildBeast;
            }
            else
            {
                $attacker = $wildBeast;
                $defender = $hero;
            }
            $eventManager->dispatch( new BeginningOfTurnEvent( $i ) );
            $eventManager->dispatch( new AttackEvent( $attacker, $defender ) );
            $eventManager->dispatch( new EndOfTurnEvent( $attacker, $defender, $i ) );
            $i++;
        }
        $reasonCode = $resolver->getGameOverReason( $hero, $wildBeast, $i );
        $eventManager->dispatch( new GameOverEvent( $hero, $wildBeast, $reasonCode ) );
    }
}
