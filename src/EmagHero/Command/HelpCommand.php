<?php

namespace EmagHero\Command;

use EmagHero\Console\Input;
use EmagHero\Console\Output;
use EmagHero\Core\AbstractCommand;

class HelpCommand extends AbstractCommand
{
    public function execute( Input $input, Output $output ) : void
    {
        $output->writeLns([
           'Available commands:',
           '-------', 
           'fight',
           'Description: Fight between the beast and the hero takes place',
           '-------',
           'help',
           'Description: Shows available commands',
           '-------',
        ]);
    }
}
