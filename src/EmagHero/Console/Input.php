<?php

namespace EmagHero\Console;

class Input
{
    private $tokens;
    
    public function __construct( array $argv )
    {
        /**
         * remove 1st arg
         */
        array_shift( $argv );
        $this->tokens = $argv;
    }
    
    public function getFirst()
    {
        if( isset( $this->tokens[0] ) )
        {
            return $this->tokens[0];
        }
    }
    
    public function isArgumentPresent() : bool
    {
        if( isset( $this->tokens[0] ) )
        {
            return true;
        }
        return false;
    }
}
