<?php

namespace EmagHero\Console;

class Output
{
    public function writeLn( $message ) : void
    {
        print $message . PHP_EOL;
    }
    
    public function writeLns( array $messages ) : void
    {
        foreach( $messages as $singleMessage )
        {
            $this->writeLn( $singleMessage );
        }
    }
}
