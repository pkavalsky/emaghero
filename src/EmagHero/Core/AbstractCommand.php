<?php

namespace EmagHero\Core;

use EmagHero\Console\Input;
use EmagHero\Console\Output;
use EmagHero\Core\DependencyInjection;

abstract class AbstractCommand
{
    abstract function execute( Input $input, Output $output ) : void;
    
    protected function getDI()
    {
        return DependencyInjection::getInstance();
    }
}
