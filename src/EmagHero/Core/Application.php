<?php

namespace EmagHero\Core;

use EmagHero\Console\Output;
use EmagHero\Core\DependencyInjection;

class Application
{
    public function run() : void
    {
        $input = InputFactory::createFromGlobal();
        $output = DependencyInjection::getInstance()->get('output');
        $commandDispatcher = new CommandDispatcher;
        $commandDispatcher->dispatch( $input, $output );
    }
}
