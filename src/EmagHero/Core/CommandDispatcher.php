<?php

namespace EmagHero\Core;

use EmagHero\Console\Input;
use EmagHero\Console\Output;

class CommandDispatcher
{
    public function dispatch( Input $input, Output $output ) : void
    {
        try
        {
            $commandClassName = 'EmagHero\\Command\\' . ucfirst( $input->getFirst() ) . 'Command';
            if( !class_exists( $commandClassName ) )
            {
                $output->writeLns([
                   'Command not found. To see available commands, type:',
                   'emaghero help',
                ]);
            }
            else
            {
                $command = new $commandClassName;
                $command->execute( $input, $output );
            }
        } 
        catch (Exception $ex) 
        {
            $exceptionHandler = new ExceptionHandler( $output, $ex );
            $exceptionHandler->render();
        }
    }
}
