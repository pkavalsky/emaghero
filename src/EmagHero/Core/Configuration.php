<?php

namespace EmagHero\Core;

class Configuration
{
    public function __construct( array $configurationArray )
    {
        foreach( $configurationArray as $singleParamKey => $paramValue )
        {
            if (is_array( $paramValue ))
            {
                $this->{ $singleParamKey } = new Configuration( $paramValue );
            }
            else
            {
                $this->{ $singleParamKey } = $paramValue;
            }
        }
    }
    
    public function __get( $propertyName )
    {
        if( property_exists($this, $propertyName ) )
        {
            return $this->property;
        }
        throw new \Exception('Property ' . $propertyName . ' does not exist in config.php');
    } 
}
