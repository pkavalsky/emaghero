<?php

namespace EmagHero\Core;

class DependencyInjection
{
    private $instances;
    private $definitions;
    public static $instance;
    
    public function setShared( $classKey, callable $definition )
    {
        if( isset ( $this->definitions[$classKey] ) )
        {
            throw new \Exception('class key: ' . $classKey . ' already exists');
        }
        $this->definitions[$classKey] = $definition;
    }
    
    public function get( $classKey )
    {
        if( isset( $this->instances[ $classKey ] ) )
        {
            return $this->instances[ $classKey ];
        }
        else if( isset( $this->definitions[ $classKey ] ) )
        {
            $instance =  $this->definitions[ $classKey ]();
            $this->instances[ $classKey ] = $instance;
            return $instance;
        }
        else
        {
            throw new \Exception('class key: ' . $classKey . ' was not found');
        }
    }
    
    public static function getInstance() : DependencyInjection
    {
        if( self::$instance instanceof DependencyInjection )
        {
            return self::$instance;
        }
        return self::$instance = new DependencyInjection();
    }
}
