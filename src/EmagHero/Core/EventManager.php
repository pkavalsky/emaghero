<?php

namespace EmagHero\Core;

use EmagHero\Subscriber\SubscriberInterface;
use EmagHero\Event\EventInterface;

class EventManager
{
    private $subscribedEventsArray;
    
    public function addSubscriber( SubscriberInterface $subscriber )
    {
        $subscribedEvents = $subscriber->getSubscribedEvents();
        foreach( $subscribedEvents as $eventName => $subscriberAction )
        {
            $this->subscribedEventsArray[ $eventName ] = [
                'subscriber' => $subscriber,
                'action' => $subscriberAction
            ];
        }
        return $this;
    }
    
    public function dispatch( EventInterface $event )
    {
        $eventName = $event::getName();
        if( isset( $this->subscribedEventsArray[ $eventName ]))
        {
            $subscriberObject = $this->subscribedEventsArray[ $eventName ]['subscriber'];
            $subscriberAction = $this->subscribedEventsArray[ $eventName ]['action'];
            return $subscriberObject->{ $subscriberAction }( $event );
        }
    }
    
    
}
