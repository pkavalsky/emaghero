<?php

namespace EmagHero\Core;

use EmagHero\Console\Output;
use EmagHero\Exception\InvalidArgumentException;
use EmagHero\Exception\RuntimeException;

class ExceptionHandler
{
    private $output;
    private $exception;
    
    public function __construct(  Output $output, \Exception $exception  ) : void
    {
        $this->output = $output;
        $this->exception = $exception;
    }
    
    public function render() : void
    {
        if( $this->ex instanceof InvalidArgumentException )
        {
            $this->output->writeLn( 'InvalidArgumentException: ' . $ex->getMessage() );
        }
        else if( $this->ex instanceof RuntimeException )
        {
            $this->output->writeLn( 'RuntimeException: ' . $ex->getMessage() );
        }
        else
        {
            $this->output->writeLn( 'Exception: ' . $ex->getMessage() );
        }
    }
}
