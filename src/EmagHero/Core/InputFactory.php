<?php

namespace EmagHero\Core;

use EmagHero\Console\Input;

class InputFactory
{
    public static function createFromGlobal() : Input
    {
        return $input = new Input( $_SERVER['argv'] );        
    }
}
