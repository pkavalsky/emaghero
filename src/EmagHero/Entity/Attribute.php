<?php

namespace EmagHero\Entity;

class Attribute
{
    const HEALTH = 1;
    const STRENGTH = 2;
    const DEFENCE = 3;
    const SPEED = 4;
    const LUCK = 5;
    
    private $value;
    private $type;
    
    public function __construct( $value, $type )
    {
       $reflectionClass = new \ReflectionClass( $this );
       $classConstants = $reflectionClass->getConstants();
       if( !in_array( $type, $classConstants ) )
       {
           throw new \Exception('type ' . $type .  ' is not valid Attribute type' );
       } 
       $this->value = $value;
       $this->type = $type;
    }
    
    public function getValue()
    {
        return $this->value;
    }
    
    public function setValue( $newValue )
    {
        $this->value = $newValue;
    }
    
    
}
