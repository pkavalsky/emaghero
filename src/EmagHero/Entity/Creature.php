<?php

namespace EmagHero\Entity;

use EmagHero\Entity\Attribute;
use EmagHero\Exception\InvalidArgumentException;
use EmagHero\Entity\Damage;

abstract class Creature
{
    private $name;
    private $health;
    private $strength;
    private $defence;
    private $speed;
    private $luck;
    private $isAttacker;
    
    public function __construct(
        string $name,    
        Attribute $health, 
        Attribute $strength, 
        Attribute $defence, 
        Attribute $speed, 
        Attribute $luck,
        $isAttacker = false    
    )
    {
        $this->name = $name;
        $this->health = $health;
        $this->strength = $strength;
        $this->defence = $defence;
        $this->speed = $speed;
        $this->luck = $luck;
        $this->isAttacker = $isAttacker;
    }
    
    public function getAttributeValue( $type )
    {
        if( $type === Attribute::HEALTH )
        {
            return $this->health->getValue();
        }
        else if( $type === Attribute::STRENGTH )
        {
            return $this->strength->getValue();
        }
        else if( $type === Attribute::DEFENCE )
        {
            return $this->defence->getValue();
        }
        else if( $type === Attribute::SPEED )
        {
            return $this->speed->getValue();
        }
        else if($type === Attribute::LUCK )
        {
            return $this->luck->getValue();
        }
        else
        {
            throw new InvalidArgumentException('Wrong input to ' . self::class .  '::getAttributeValue method');
        }
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getIsAttacker()
    {
        return $this->isAttacker;
    }
    
    public function setIsAttacker( $isAttacker )
    {
        $this->isAttacker = $isAttacker;
    }
    
    public function receiveDamage( Damage $damage )
    {
        $healthPoints = $this->health->getValue();
        $damagePoints = $damage->getDamagePoints();
        if( $healthPoints <= $damagePoints )
        {
            $healthPoints = 0;
        }
        else
        {
            $healthPoints = $healthPoints - $damagePoints;
        }
        $this->health->setValue( $healthPoints );
    }
}
