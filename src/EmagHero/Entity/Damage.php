<?php

namespace EmagHero\Entity;

class Damage extends AbstractEntity
{
    private $damagePoints;
    
    public function __construct ( int $damagePoints )
    {
        $this->damagePoints = $damagePoints;
    }
    
    public function getDamagePoints()
    {
        return $this->damagePoints;
    }
    
    public function setDamagePoints( $damagePoints )
    {
        $this->damagePoints = $damagePoints;
    }
}
