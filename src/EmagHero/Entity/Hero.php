<?php

namespace EmagHero\Entity;

use EmagHero\Entity\Skill\SkillInterface;

class Hero extends Creature
{
    private $skills = array();
    
    public function addSkill(SkillInterface $skill )
    {
        $this->skills[] = $skill;
    }
    
    public function getSkills()
    {
        return $this->skills;
    }
}
