<?php

namespace EmagHero\Entity\Skill;

use EmagHero\Event\BeforeDamageDoneEvent;
use EmagHero\Core\DependencyInjection;

class MagicShield implements SkillInterface
{
    public function getChance() : int
    {
        return 20;
    }
    
    public function getType() : int
    {
        return self::DEFENSIVE;
    }
    
    public function getDisplayName() : string
    {
        return 'Magic Shield';
    }
    
    public function beforeDamageDone( BeforeDamageDoneEvent $event )
    {
        $output = DependencyInjection::getInstance()->get('output');
        $damage = $event->getDamage();
        $output->writeLn( 'Damage done goes down from '.$damage->getDamagePoints().' to '.floor ( $damage->getDamagePoints() / 2 ) );
        $damage->setDamagePoints( ( $damage->getDamagePoints() / 2 ) );
    }
}
