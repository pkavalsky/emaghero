<?php

namespace EmagHero\Entity\Skill;

use EmagHero\Event\AfterDamageDoneEvent;
use EmagHero\Core\DependencyInjection;
use EmagHero\Event\AttackEvent;

class RapidStrike implements SkillInterface
{   
    public function getChance() : int
    {
        return 10;
    }
    
    public function getType() : int
    {
        return self::OFFENSIVE;
    }
    
    public function getDisplayName() : string
    {
        return 'Rapid Strike';
    }
    
    public function afterDamageDone( AfterDamageDoneEvent $event  )
    {
        $di = DependencyInjection::getInstance();
        $output = $di->get('output');
        $eventManager = $di->get('eventmanager');
        $attacker = $event->getAttacker();
        $defender = $event->getDefender();
        $output->writeLn('Performing extra attack now!');
        $eventManager->dispatch( new AttackEvent( $attacker, $defender ) );
    }
}
