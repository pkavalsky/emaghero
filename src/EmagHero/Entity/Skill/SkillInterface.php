<?php

namespace EmagHero\Entity\Skill;

interface SkillInterface
{
    public function getChance() : int;
    
    public function getType() : int;
    
    public function getDisplayName() : string;
}
