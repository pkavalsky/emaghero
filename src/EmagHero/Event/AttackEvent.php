<?php

namespace EmagHero\Event;

use EmagHero\Entity\Creature;

class AttackEvent implements EventInterface
{
    private $attacker;
    private $defender;
    
    public function __construct( Creature $attacker, Creature $defender )
    {
        $this->attacker = $attacker;
        $this->defender = $defender;
    }
    
    public static function getName() : string
    {
        return 'attack.performed';
    }
    
    public function getAttacker() : Creature
    {
        return $this->attacker;
    }

    public function getDefender() : Creature
    {
        return $this->defender;
    }
}
