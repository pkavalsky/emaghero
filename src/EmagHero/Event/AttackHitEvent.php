<?php

namespace EmagHero\Event;

use EmagHero\Entity\Creature;

class AttackHitEvent implements EventInterface
{
    private $attacker;
    private $defender;
    
    public function __construct( Creature $attacker, Creature $defender )
    {
        $this->attacker = $attacker;
        $this->defender = $defender;
    }
    
    public static function getName() : string
    {
        return 'luck.test.failed';
    }
    
    public function getAttacker()
    {
        return $this->attacker;
    }

    public function getDefender()
    {
        return $this->defender;
    }
}
