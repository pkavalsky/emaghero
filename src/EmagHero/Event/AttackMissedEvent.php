<?php

namespace EmagHero\Event;

use EmagHero\Entity\Creature;

class AttackMissedEvent implements EventInterface 
{
    private $defender;
    
    public function __construct( Creature $defender )
    {
        $this->defender = $defender;
    }
    
    public static function getName() : string
    {
        return 'luck.test.success';
    }
    
    public function getDefender()
    {
        return $this->defender;
    }
}
