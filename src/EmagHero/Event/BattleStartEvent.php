<?php

namespace EmagHero\Event;

use EmagHero\Entity\Creature;

class BattleStartEvent implements EventInterface
{
    private $firstPlayer;
    private $secondPlayer;
    
    public function __construct( Creature $firstPlayer, Creature $secondPlayer )
    {
        $this->firstPlayer = $firstPlayer;
        $this->secondPlayer = $secondPlayer;
    }
    
    public static function getName() : string
    {
        return 'battle.start';
    }
    
    public function getFirstPlayer()  : Creature
    {
        return $this->firstPlayer;
    }

    public function getSecondPlayer()  : Creature
    {
        return $this->secondPlayer;
    }
}
