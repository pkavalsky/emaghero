<?php

namespace EmagHero\Event;

use EmagHero\Entity\Damage;
use EmagHero\Entity\Creature;

class BeforeDamageDoneEvent implements EventInterface
{
    private $damage;
    private $defender;
    
    public function __construct( Creature $defender, Damage $damage )
    {
        $this->damage = $damage;
        $this->defender = $defender;
    }
    
    public static function getName() : string
    {
        return 'before.damage.done';
    }
    
    public function getDamage() : Damage
    {
        return $this->damage;
    }
    
    public function getDefender()
    {
        return $this->defender;
    }
}
