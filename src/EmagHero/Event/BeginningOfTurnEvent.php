<?php

namespace EmagHero\Event;

class BeginningOfTurnEvent implements EventInterface
{
    private $turnNumber;
    
    public function __construct(  int $iteration )
    {
        $this->turnNumber = $iteration;
    }
    
    public static function getName() : string
    {
        return 'beginningofturn';
    }

    public function getTurnNumber() : int
    {
        return $this->turnNumber;
    }
}
