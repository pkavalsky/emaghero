<?php

namespace EmagHero\Event;

use EmagHero\Entity\Creature;
use EmagHero\Entity\Damage;

class DamageDoneEvent implements EventInterface
{
    private $defender;
    private $damage;
    
    public function __construct(Creature $defender, Damage $damage)
    {
        $this->defender = $defender;
        $this->damage = $damage;
    }
    
    public static function getName() : string
    {
        return 'damage.done';
    }
    
    public function getDefender() : Creature
    {
        return $this->defender;
    }

    public function getDamage() : Damage
    {
        return $this->damage;
    }
}
