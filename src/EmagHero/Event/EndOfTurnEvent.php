<?php

namespace EmagHero\Event;

use EmagHero\Entity\Creature;

class EndOfTurnEvent implements EventInterface
{
    private $attacker;
    private $defender;
    private $turnNumber;
    
    public function __construct( Creature $attacker, Creature $defender, int $turnNum)
    {
        $this->attacker = $attacker;
        $this->defender = $defender;
        $this->turnNumber = $turnNum;
    }
    
    public static function getName() : string
    {
        return 'endofturn';
    }
    
    public function getAttacker()  : Creature
    {
        return $this->attacker;
    }

    public function getDefender()  : Creature
    {
        return $this->defender;
    }

    public function getTurnNumber() : int
    {
        return $this->turnNumber;
    }
}
