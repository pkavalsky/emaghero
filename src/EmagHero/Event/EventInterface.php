<?php

namespace EmagHero\Event;

interface EventInterface
{
   public static function getName() : string;
}
