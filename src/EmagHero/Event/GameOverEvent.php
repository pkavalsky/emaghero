<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace EmagHero\Event;

/**
 * Description of GameOverEvent
 *
 * @author pkowalski
 */
class GameOverEvent implements EventInterface
{
    private $gameOverReason;
    private $firstPlayer;
    private $secondPlayer;
    
    public function __construct( $firstPlayer, $secondPlayer, $gameOverReasonCode )
    {
        $this->gameOverReason = $gameOverReasonCode;
        $this->firstPlayer = $firstPlayer;
        $this->secondPlayer = $secondPlayer;
    }
    
    public static function getName() : string
    {
        return 'game.over';
    }
    
    public function getGameOverReason()
    {
        return $this->gameOverReason;
    }

    public function getFirstPlayer()
    {
        return $this->firstPlayer;
    }

    public function getSecondPlayer()
    {
        return $this->secondPlayer;
    }
}
