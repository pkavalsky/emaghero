<?php

namespace EmagHero\Event;

use EmagHero\Entity\Creature;

class SpeedTestSuccessEvent implements EventInterface
{
    private $speedTestWinner;
    
    public function __construct( Creature $speedTestWinner )
    {
        $this->speedTestWinner = $speedTestWinner;
    }
    
    public static function getName() : string
    {
        return 'speed.test.success';
    }
    
    public function getSpeedTestWinner()
    {
        return $this->speedTestWinner;
    }


}
