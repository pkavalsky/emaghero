<?php

namespace EmagHero\Mapper;

use EmagHero\Core\Configuration;
use EmagHero\Entity\Attribute;

class AttributeMapper
{
    const HERO = 1;
    const WILD_BEAST = 2;
    
    public function create($minValue, $maxValue, $type  ) : Attribute
    {
        $num = mt_rand($minValue, $maxValue);
        $attribute = new Attribute( $num , $type);
        return $attribute;
    }
    
    public function createFromConfig( Configuration $attributesConf )
    {
        $health = $this->create( $attributesConf->health->min, $attributesConf->health->max, Attribute::HEALTH );
        $strength = $this->create( $attributesConf->strength->min, $attributesConf->strength->max, Attribute::STRENGTH );
        $defence = $this->create( $attributesConf->defence->min, $attributesConf->defence->max, Attribute::DEFENCE );
        $speed = $this->create( $attributesConf->speed->min, $attributesConf->speed->max, Attribute::SPEED );
        $luck = $this->create( $attributesConf->luck->min, $attributesConf->luck->max, Attribute::LUCK );
        return [
            'health' => $health,
            'strength' => $strength,
            'defence' => $defence,
            'speed' => $speed,
            'luck' => $luck
        ];
    }
}
