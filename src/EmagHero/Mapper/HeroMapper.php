<?php

namespace EmagHero\Mapper;

use EmagHero\Core\Registry;
use EmagHero\Mapper\AttributeMapper;
use EmagHero\Entity\Hero;
use EmagHero\Core\DependencyInjection;

class HeroMapper
{
    public function create()
    {
        $heroConfig = DependencyInjection::getInstance()->get('config')->hero;
        $attributeMapper = new AttributeMapper;
        $attributes = $attributeMapper->createFromConfig( $heroConfig->attributes );
        $hero = new Hero(
            $heroConfig->name, 
            $attributes['health'], 
            $attributes['strength'], 
            $attributes['defence'], 
            $attributes['speed'], 
            $attributes['luck'] 
        );
        foreach( (array)$heroConfig->skills as $singleSkillClass )
        {
            $hero->addSkill( new  $singleSkillClass);
        }
        return $hero; 
    }
}
