<?php

namespace EmagHero\Mapper;

use EmagHero\Entity\WildBeast;
use EmagHero\Core\DependencyInjection;

class WildBeastMapper
{
    public function create()
    {
        $wildBeastConfig = DependencyInjection::getInstance()->get('config')->wildbeast;
        $attributeMapper = new AttributeMapper;
        $attributes = $attributeMapper->createFromConfig( $wildBeastConfig->attributes );
        $hero = new WildBeast(
            $wildBeastConfig->name, 
            $attributes['health'], 
            $attributes['strength'], 
            $attributes['defence'], 
            $attributes['speed'], 
            $attributes['luck'] 
        );
        return $hero; 
    }
}
