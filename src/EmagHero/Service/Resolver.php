<?php

namespace EmagHero\Service;

use EmagHero\Entity\Attribute;
use EmagHero\Entity\Creature;

class Resolver
{
    const MAX_TURN_REACHED = 1;
    const PLAYER_DEAD = 2;
    
    public function compareAttribute( Creature $firstPlayer, Creature $secondPlayer, $attributeType )
    {
        $firstPlayerAttrVal = $firstPlayer->getAttributeValue( $attributeType );
        $secondPlayerAttrVal = $secondPlayer->getAttributeValue( $attributeType );
        if( $firstPlayerAttrVal > $secondPlayerAttrVal )
        {
            return $firstPlayer;
        }
        else if( $firstPlayerAttrVal < $secondPlayerAttrVal )
        {
            return $secondPlayer;
        }
        else
        {
            return false;
        }
    }
    
    public function isGameOver( Creature $firstPlayer, Creature $secondPlayer, int $i)
    {
        if( $firstPlayer->getAttributeValue( Attribute::HEALTH ) <= 0 ||
            $secondPlayer->getAttributeValue( Attribute::HEALTH ) <= 0  )
        {
            return true;
        }
        else if( $i > 20 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function getGameOverReason( Creature $firstPlayer, Creature $secondPlayer, int $i )
    {
        if( $firstPlayer->getAttributeValue( Attribute::HEALTH ) <= 0 ||
            $secondPlayer->getAttributeValue( Attribute::HEALTH ) <= 0  )
        {
            self::PLAYER_DEAD;
        }
        else if( $i > 20 )
        {
            self::MAX_TURN_REACHED;
        }
        else
        {
            return false;
        }
    }
    
    public function flipCoin()
    {
        return mt_rand(0, 1);
    }
    
    public function resolveDamagePoints( Creature $attacker, Creature $defender )
    {
        // Damage = Attacker strength – Defender defence
        $attackerStrength = $attacker->getAttributeValue( Attribute::STRENGTH );
        $defenderDefence = $defender->getAttributeValue( Attribute::DEFENCE );
        $damagePoints = $attackerStrength - $defenderDefence;
        if( $damagePoints < 0 )
        {
            return 0;
        }
        else
        {
            return $damagePoints;
        }    
    }
}
