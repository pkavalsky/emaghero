<?php

namespace EmagHero\Subscriber;

use EmagHero\Event\AttackHitEvent;
use EmagHero\Event\AttackMissedEvent;
use EmagHero\Core\DependencyInjection;
use EmagHero\Entity\Attribute;
use EmagHero\Event\BeforeDamageDoneEvent;
use EmagHero\Event\DamageDoneEvent;
use EmagHero\Event\AfterDamageDoneEvent;
use EmagHero\Entity\Damage;

class AttackSubscriber implements SubscriberInterface
{
    private $output;
    private $di;
    
    public function __construct()
    {
        $this->di = DependencyInjection::getInstance();
        $this->output = $this->di->get('output');
    }
    
    public function getSubscribedEvents() : array
    {
        return [
            AttackHitEvent::getName() => 'onAttackHit',
            AttackMissedEvent::getName() => 'onAttackMissed'
        ];
    }
    
    public function onAttackHit( AttackHitEvent $event )
    {
        $attacker = $event->getAttacker();
        $defender = $event->getDefender();
        $resolver = $this->di->get('resolver');
        $eventManager = $this->di->get('eventmanager');        
        $this->output->writeLns([
            'Hit successful',
            $attacker->getName() . ' has ' . $attacker->getAttributeValue( Attribute::STRENGTH ) . ' strength points ',
            $defender->getName() . ' has ' . $defender->getAttributeValue( Attribute::DEFENCE ) . ' defence points ',
            $defender->getName() . ' has ' . $defender->getAttributeValue( Attribute::HEALTH ) . ' health points '
        ]);
        $damage = new Damage( $resolver->resolveDamagePoints( $attacker, $defender ) );
        $eventManager->dispatch( new BeforeDamageDoneEvent( $defender, $damage ) );
        $this->output->writeLn( $defender->getName().' receives '.$damage->getDamagePoints().' damage points');
        $eventManager->dispatch( new DamageDoneEvent( $defender, $damage ) );
        $this->output->writeLn( $defender->getName().' has now '.$defender->getAttributeValue( Attribute::HEALTH ).' health points');
        $eventManager->dispatch( new AfterDamageDoneEvent( $attacker, $defender ) );
    }
    
    public function onAttackMissed( AttackMissedEvent $event )
    {
        $defender = $event->getDefender();
        $this->output->writeLn('Luck test successfull, no damage done to '.$defender->getName() );
    }
}
