<?php

namespace EmagHero\Subscriber;

use EmagHero\Event\AttackEvent;
use EmagHero\Core\DependencyInjection;
use EmagHero\Event\BattleStartEvent;
use EmagHero\Entity\Attribute;
use EmagHero\Entity\Creature;
use EmagHero\Event\EndOfTurnEvent;
use EmagHero\Event\BeginningOfTurnEvent;
use EmagHero\Event\GameOverEvent;
use EmagHero\Service\Resolver;
use EmagHero\Event\AttackHitEvent;
use EmagHero\Event\AttackMissedEvent;
use EmagHero\Event\SpeedTestSuccessEvent;
use EmagHero\Event\SpeedTestDrawEvent;

class BattleSubscriber implements SubscriberInterface
{
    private $output;
    private $di;
    
    public function __construct()
    {
        $this->di = DependencyInjection::getInstance();
        $this->output = $this->di->get('output');
    }
    
    public function getSubscribedEvents() : array
    {
        return [
            BattleStartEvent::getName() => 'onBattleStart',
            BeginningOfTurnEvent::getName() => 'onTurnBegin',
            AttackEvent::getName() => 'onAttackPerformed',
            EndOfTurnEvent::getName() => 'onTurnFinished',
            GameOverEvent::getName() => 'onGameOver',
        ];
    }
   
    public function onBattleStart( BattleStartEvent $battleStart )
    {
        $resolver = $this->di->get('resolver');
        $eventManager = $this->di->get('eventmanager');
        $firstPlayer = $battleStart->getFirstPlayer();
        $secondPlayer = $battleStart->getSecondPlayer();
        $this->output->writeLns([
            'Battle starts - 2 opponents stand to battle',
            'Resolving who attacks first by speed...',
            $firstPlayer->getName() . ' has ' . $firstPlayer->getAttributeValue( Attribute::SPEED ) . ' speed points',
            $secondPlayer->getName() . ' has ' . $secondPlayer->getAttributeValue( Attribute::SPEED ) . ' speed points',
        ]);
        $fasterPlayer = $resolver->compareAttribute( $firstPlayer, $secondPlayer, Attribute::SPEED );
        if( $fasterPlayer instanceof Creature )
        {
            $eventManager->dispatch( new SpeedTestSuccessEvent( $fasterPlayer ) );
        }
        else
        {
            $eventManager->dispatch( new SpeedTestDrawEvent( $firstPlayer, $secondPlayer ) );
        }
        
    }
    
    public function onTurnBegin( BeginningOfTurnEvent $event )
    {
        $turnNumber = $event->getTurnNumber();
        $this->output->writeLn('');
        $this->output->writeLn('===================================');
        $this->output->writeLn('Turn ' . $turnNumber . ' starts');
    }
    
    public function onTurnFinished( EndOfTurnEvent $event )
    {
        $attacker = $event->getAttacker();
        $defender = $event->getDefender();
        /**
         * flip roles
         */
        $attacker->setIsAttacker(false);
        $defender->setIsAttacker(true);
        $turnNumber = $event->getTurnNumber();
        $this->output->writeLn('Turn ' . $turnNumber . ' finished');
        $this->output->writeLn('===================================');
        
    }
    
    public function onAttackPerformed( AttackEvent $attackEvent )
    {
        $eventmanager = $this->di->get('eventmanager');
        $attacker = $attackEvent->getAttacker();
        $defender = $attackEvent->getDefender();
        $defenderLuckPoints = $defender->getAttributeValue( Attribute::LUCK );
        $this->output->writeLns([
            'Attacker is '.$attacker->getName(),
            'Defender is '.$defender->getName(),
            'starting attack',
            'Checking if attack hits '.$defender->getName(),
            $defender->getName().' has '.$defenderLuckPoints.' luck points '
        ]);
        $luckRoll =  mt_rand(1, 100);
        $this->output->writeLn('Luck test equals: ' . $luckRoll);
        if( ($defenderLuckPoints > 0 && $luckRoll < $defenderLuckPoints ) ||  
            $defenderLuckPoints === 100 )
        {
            $eventmanager->dispatch( new AttackMissedEvent( $defender ) );
        }
        else
        {
            $eventmanager->dispatch( new AttackHitEvent($attacker, $defender) );
        }
    }
    
    public function onGameOver( GameOverEvent $event )
    {
        $firstPlayer = $event->getFirstPlayer();
        $secondPlayer = $event->getSecondPlayer();
        $reasonCode = $event->getGameOverReason();
        if( $reasonCode === Resolver::MAX_TURN_REACHED )
        {
            $this->output->writeLn('Game over due to max turn reached');
        }
        else
        {
            if( $firstPlayer->getAttributeValue( Attribute::HEALTH ) === 0 )
            {
                $this->output->writeLns([
                    'Game over because '.$firstPlayer->getName().' has died',
                    $secondPlayer->getName().' is winner!',
                ]);
                
            }
            else
            {
                $this->output->writeLns([
                    '','Game over because ' . $secondPlayer->getName() . ' has died',
                    $firstPlayer->getName().' is winner!',
                ]);
            }
        }
    }
    
    
}
