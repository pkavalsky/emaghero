<?php

namespace EmagHero\Subscriber;

use EmagHero\Event\BeforeDamageDoneEvent;
use EmagHero\Event\DamageDoneEvent;
use EmagHero\Event\AfterDamageDoneEvent;
use EmagHero\Core\DependencyInjection;
use EmagHero\Entity\Hero;

class DamageSubscriber implements SubscriberInterface
{
    private $output;
    private $di;
    
    public function __construct()
    {
        $this->di = DependencyInjection::getInstance();
        $this->output = $this->di->get('output');
    }
    
    public function getSubscribedEvents() : array
    {
        return [
            BeforeDamageDoneEvent::getName() => 'beforeDamageDone',
            DamageDoneEvent::getName() => 'onDamageDone',
            AfterDamageDoneEvent::getName() => 'afterDamageDone'
        ];
    }
    
    public function beforeDamageDone( BeforeDamageDoneEvent $event )
    {
        $defender = $event->getDefender();
        /**
         * if defender is a hero ( = has skills ) - iterate through them and call their beforeDamageDone if defined
         */
        if ( $defender instanceof Hero )
        {
            foreach( $defender->getSkills() as $skill )
            {
                if( method_exists( $skill, 'beforeDamageDone') )
                {              
                    $chanceToUse = $skill->getChance();
                    $this->output->writeLn('There is '.$chanceToUse.'% chance that '.$defender->getName().' uses '.$skill->getDisplayName().' skill' );
                    $roll = mt_rand( 1 , 100);
                    $this->output->writeLn('Roll equals: '.$roll);
                    if( $roll <= $skill->getChance() )
                    {
                        $this->output->writeLn($defender->getName().' successfully casted '.$skill->getDisplayName().' skill!');
                        $skill->beforeDamageDone( $event );
                    }
                    else
                    {
                        $this->output->writeLn('Failed to use '.$skill->getDisplayName().' skill');
                    }
                }
            }
        }
    }
    
    public function onDamageDone( DamageDoneEvent $event )
    {
        $damage = $event->getDamage();
        $defender = $event->getDefender();
        $defender->receiveDamage($damage);
    }
    
    public function afterDamageDone( AfterDamageDoneEvent $event )
    {
        $attacker = $event->getAttacker();
        /**
         * if defender is a hero ( = has skills ) - iterate through them and call afterDamageDone if defined
         */
        if ( $attacker instanceof Hero )
        {
            foreach( $attacker->getSkills() as $skill )
            {
                if( method_exists( $skill, 'afterDamageDone') )
                {              
                    $chanceToUse = $skill->getChance();
                    $this->output->writeLn('There is '.$chanceToUse.'% chance that '.$attacker->getName().' uses '.$skill->getDisplayName().' skill' );
                    $roll = mt_rand( 1 , 100);
                    $this->output->writeLn('Roll equals: '.$roll);
                    if( $roll <= $skill->getChance() )
                    {
                        $this->output->writeLn($attacker->getName().' successfully casted '.$skill->getDisplayName().' skill!');
                        $skill->afterDamageDone( $event );
                    }
                    else
                    {
                        $this->output->writeLn('Failed to use '.$skill->getDisplayName().' skill');
                    }
                }
            }
        }
    }
}
