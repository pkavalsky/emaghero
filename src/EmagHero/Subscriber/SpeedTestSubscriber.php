<?php

namespace EmagHero\Subscriber;

use EmagHero\Event\SpeedTestSuccessEvent;
use EmagHero\Core\DependencyInjection;
use EmagHero\Event\SpeedTestDrawEvent;
use EmagHero\Entity\Attribute;

class SpeedTestSubscriber implements SubscriberInterface
{
    private $output;
    private $di;
    
    public function __construct()
    {
        $this->di = DependencyInjection::getInstance();
        $this->output = $this->di->get('output');
    }
    
    public function getSubscribedEvents() : array
    {
        return [
            SpeedTestSuccessEvent::getName() => 'onSpeedTestSuccess',
            SpeedTestDrawEvent::getName() => 'onSpeedTestDraw'
        ];
    }
    
    public function onSpeedTestSuccess( SpeedTestSuccessEvent $event )
    {
        $fasterPlayer = $event->getSpeedTestWinner();
        $this->output->writeLn($fasterPlayer->getName() . ' has higher speed and is attacker');
        $fasterPlayer->setIsAttacker( true );
    }
    
    public function onSpeedTestDraw( SpeedTestDrawEvent $event )
    {
        $firstPlayer = $event->getFirstPlayer();
        $secondPlayer = $event->getSecondPlayer();
        $resolver = $this->di->get('resolver');
        $this->output->writeLns([
            'Both players have same speed - resolving who attacks first by luck',
            $firstPlayer->getName() . ' has ' . $firstPlayer->getAttributeValue( Attribute::LUCK ) . ' luck points',
            $secondPlayer->getName() . ' has ' . $secondPlayer->getAttributeValue( Attribute::LUCK ) . ' luck points',
        ]);
        $luckierPlayer = $resolver->compareAttribute( $firstPlayer, $secondPlayer, Attribute::LUCK );
        if( $luckierPlayer instanceof Creature )
        {
            $this->output->writeLn( $luckierPlayer->getName() . ' has higher luck and is attacker' );
            $luckierPlayer->setIsAttacker( true );
        }
        /**
         * lucks are same - flip a coin
         */
        else
        {
            $this->output->writeLns([
                'Both players have same luck! Flipping a coin.',
                'Tails for ' . $firstPlayer->getName(),
                'Heads for ' . $secondPlayer->getName(),
            ]);
            $outcome = $resolver->flipCoin();
            if( $outcome === 0)
            {
                $this->output->writeLn('Tails! ' . $firstPlayer->getName() . ' attacks first');
                $firstPlayer->setIsAttacker( true );
            }
            else
            {
                $this->output->writeLn('Heads!! ' . $secondPlayer->getName() . ' attacks first');
                $secondPlayer->setIsAttacker( true );
            }
        }
    }
}
