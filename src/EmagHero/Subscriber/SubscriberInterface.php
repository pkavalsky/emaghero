<?php

namespace EmagHero\Subscriber;

interface SubscriberInterface
{
    public function getSubscribedEvents() : array;
}
