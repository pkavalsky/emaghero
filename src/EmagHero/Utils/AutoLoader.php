<?php

namespace EmagHero\Utils\AutoLoader;

class AutoLoader
{
    private $baseDir;
   
    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }
    
    public function setBaseDir( $baseDir )
    {
        $this->baseDir = $baseDir;
    }
    
    public function loadClass( $classNameWithNamespaces )
    {
        $classWithNamespacesArray = explode( '\\', $classNameWithNamespaces );
        $className = end( $classWithNamespacesArray );
        array_shift( $classWithNamespacesArray );
        $classFilePath = '';
        for( $i=0; $i < count( $classWithNamespacesArray ); $i ++)
        {
            $classFilePath .= DIRECTORY_SEPARATOR .  $classWithNamespacesArray[$i];
        }
        $classFilePath = $this->baseDir . $classFilePath . '.php';
        if( !file_exists( $classFilePath ))
        {
            return false;
        }
        require_once $classFilePath;
    }
}