<?php

require_once __DIR__ . '/Utils/AutoLoader.php';
$autoLoader = new \EmagHero\Utils\AutoLoader\AutoLoader;
$autoLoader->setBaseDir( __DIR__ );
$autoLoader->register();
require_once __DIR__ . '/bootstrap.php';