<?php

use EmagHero\Core\DependencyInjection;
use EmagHero\Console\Output;
use EmagHero\Core\EventManager;
use EmagHero\Subscriber\BattleSubscriber;
use EmagHero\Service\Resolver;
use EmagHero\Subscriber\AttackSubscriber;
use EmagHero\Subscriber\SpeedTestSubscriber;
use EmagHero\Subscriber\DamageSubscriber;

$di = DependencyInjection::getInstance();

$di->setShared( 'config', function() {
    return include __DIR__ . '/config.php';
});

$di->setShared( 'output', function() {
    return new Output;
});

$di->setShared( 'eventmanager', function(){
    $eventManager = new EventManager;
    $eventManager->addSubscriber( new BattleSubscriber() )
                 ->addSubscriber( new AttackSubscriber )
                 ->addSubscriber( new SpeedTestSubscriber )
                 ->addSubscriber( new DamageSubscriber);
    return $eventManager;
});

$di->setShared('resolver', function(){
    return new Resolver;
});


