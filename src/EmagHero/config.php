<?php
return new \EmagHero\Core\Configuration([
   'hero' => [
       'name' => 'Orderus',
       'attributes' => [
           'health' => [
               'min' => 70,
               'max' => 100
           ],
           'strength' => [
               'min' => 70,
               'max' => 80
           ],
           'defence' => [
               'min' => 45,
               'max' => 55
           ],
           'speed' => [
               'min' => 40,
               'max' => 50
           ],
           'luck' => [
               'min' => 10,
               'max' => 30
           ]
       ],
       'skills' => [
           '\\EmagHero\\Entity\\Skill\\MagicShield',
           '\\EmagHero\\Entity\\Skill\\RapidStrike'
       ]
   ],
   'wildbeast' => [
       'name' => 'Wild Beast',
       'attributes' => [
           'health' => [
               'min' => 60,
               'max' => 90
           ],
           'strength' => [
               'min' => 60,
               'max' => 90
           ],
           'defence' => [
               'min' => 40,
               'max' => 60
           ],
           'speed' => [
               'min' => 40,
               'max' => 60
           ],
           'luck' => [
               'min' => 25,
               'max' => 40
           ]
       ]
   ],
]);
